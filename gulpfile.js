const gulp = require('gulp');
const uglifycss = require('gulp-uglifycss');

gulp.task('css', function() {
    gulp.src('./stylesheets/*.css')
        .pipe(
            uglifycss({
                maxLinelen: 80,
                uglyComments: true
            })
        )
        .pipe(gulp.dest('./dist/'));
});
