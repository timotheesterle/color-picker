function id(el) {
    return document.getElementById(el);
}

id('color1').style.backgroundColor = `#FFFFFF`;
id('color2').style.backgroundColor = `#FFFFFF`;

function updateValue(element, colorNum) {
    id(element + colorNum).addEventListener('input', function() {
        id('value_' + element + colorNum).innerHTML = this.value;

        id('color' + colorNum).style.backgroundColor = `rgb(${
            id('red' + colorNum).value
        }, ${id('green' + colorNum).value}, ${id('blue' + colorNum).value})`;

        id('gradient').style.backgroundImage = `linear-gradient(to right, ${
            id('color1').style.backgroundColor
        }, ${id('color2').style.backgroundColor})`;
    });
}

updateValue('red', 1);
updateValue('green', 1);
updateValue('blue', 1);
updateValue('red', 2);
updateValue('green', 2);
updateValue('blue', 2);
